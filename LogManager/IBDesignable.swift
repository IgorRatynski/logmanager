//
//  IBDesignable.swift
//  Pusher
//
//  Created by Igor Ratynski on 16.09.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

@IBDesignable
class RoundableButton: UIButton {
    
    @IBInspectable var cornerRadiusOfButton : CGFloat = 0 { didSet { layer.cornerRadius = cornerRadiusOfButton } }
    
    @IBInspectable var borderWidth: CGFloat {
        set { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set { if let uiColor = newValue { layer.borderColor = uiColor.cgColor } }
        get { guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
