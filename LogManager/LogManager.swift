//
//  LogManager.swift
//  LogManager
//
//  Created by Igor Ratynski on 28/09/2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

class Log {
    
    internal typealias Message = [String: String]
    
    static private let concurrentQueue = DispatchQueue(label:"com.customLogManager.queue")
    
    static private var messages: [Message] = []
    static private var totalBytesSize: Int = 0 { didSet { print(totalBytesSize) } }
    
    static func message(_ message: Message, insertAtStart: Bool = false) {
        func sendToServer() {
            if NetworkManager.sharedInstanse.isConnected {
                NetworkManager.sharedInstanse.SendLogRequest(logString: messagesToServerFormat())
                removeMessages()
            }
        }
        
        func cutOldMessages() {
            let initalCutSize = totalBytesSize - kb300
            var cuttingSize = initalCutSize
            
            while cuttingSize > 0 {
                cuttingSize -= messages[0].sizeInBytes()
                messages.remove(at: 0)
            }
            
            totalBytesSize -= initalCutSize + cuttingSize
        }
        
        DispatchQueue.global(qos: .default).async { concurrentQueue.sync {
            
            messages.append(message)
            totalBytesSize += message.sizeInBytes()
            print(messagesToServerFormat())
            switch totalBytesSize {
            case kb100...kb300: sendToServer()
            case kb300...: cutOldMessages()
            default: break
            }
        }
        
    }}
    
    // MARK: - Private
    
    static private func removeMessages() {
        messages.removeAll()
        totalBytesSize = 0
    }
    
    static private func messagesToServerFormat() -> String {
        return ""
    }
    
    private init() { }
}

internal let kb100 = 102400
private  let kb300 = 307200


fileprivate extension Dictionary where Key: Hashable, Value: Hashable {
    
    fileprivate func sizeInBytes() -> Int {
        return Array(keys).sizeInBytes() + Array(values).sizeInBytes()
    }
    
}

 fileprivate extension Array where Element: Hashable {
    
     fileprivate func sizeInBytes() -> Int {
        var byteSize: Int = 0
        
        self.forEach { byteSize += ($0 as! String).utf8.count }
        return byteSize
    }
}
