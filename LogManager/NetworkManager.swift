//
//  NetworkManager.swift
//  LogManager
//
//  Created by Igor Ratynski on 28/09/2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static let sharedInstanse = NetworkManager()
    
    private(set) var isConnected: Bool = false
    
    public func SendLogRequest(logString: String) {
    
    }
    
    // MARK: - Private
    
    private init() { }
}
