//
//  ViewController.swift
//  LogManager
//
//  Created by Igor Ratynski on 28/09/2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    // MARK: - Actions
    
    @IBAction func addMessages(_ sender: Any) {
        
        Log.message(["": String(repeating: "#", count: kb100)])
    }
    
    @IBAction func log(_ sender: Any) {
        Log.message(["K": textField.text ?? ""])
        textField.text = ""
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

